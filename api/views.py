from django.shortcuts import render
from rest_framework import generics
from articles.models import Article
from .serializers import ArticleSerializer, UserSerializer
from django.contrib.auth import get_user_model
from .permissions import IsAuthorOrReadOnly
# Create your views here.
class ArticleApiView(generics.ListCreateAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
class DetailApiView(generics.RetrieveUpdateDestroyAPIView):
    
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
